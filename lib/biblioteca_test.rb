# encoding: utf-8

require 'minitest/autorun'
require_relative './biblioteca.rb'
require_relative './livro.rb'

class LivrariaTest < Minitest::Test

  def setup
    @biblioteca = Livaria::Biblioteca.new
    @livro_test = Livaria::Livro.new "TDD", "Mauricio Aniche", "123454", 247, 10, :testes
  end

  def teardown
    @biblioteca = nil
  end

  def test_add_livros
    livro_web = Livaria::Livro.new "Itacir Pompeu", "654321", 247, 10, :web

    @biblioteca.add_livros(@livro_test)
    @biblioteca.add_livros(livro_web)

    assert_equal @biblioteca.livros.length, 2 
  end

  def test_get_livro_by_category
    @biblioteca.add_livros(@livro_test)

    @biblioteca.livros_por_categoria :testes do |livro|
      assert_equal @livro_test.preco, livro.preco
      assert_equal @livro_test.isbn, livro.isbn 
      assert_equal @livro_test.categoria, livro.categoria 
    end
  end

  def test_read_title_by_proc
    @biblioteca.add_livros(@livro_test)

    test_category = Proc.new do |livro|
      assert_equal @livro_test.categoria, livro.categoria
    end

    @biblioteca.livros_por_categoria :testes, &test_category
  end

  def test_read_title_by_lambda
    @biblioteca.add_livros(@livro_test)

    test_title = lambda do |livro|
      assert_equal @livro_test.titulo, livro.titulo
    end

    @biblioteca.livros_por_categoria :testes, &test_title 

  end

end
