# encoding: utf-8

require 'minitest/autorun'
require_relative './livro.rb'

class LivroTest < Minitest::Test

  def setup
    @teste_e_design = Livaria::Livro.new "TDD", "Mauricio Aniche", "123454", 247, 10, :testes
  end

  def teardown
    @teste_e_design = nil
  end

  def test_initialize_livro 
    refute_nil @teste_e_design, 'can\'t not be nil'
  end

  def test_to_s_result
    result_data_to_s =  'Autor: Mauricio Aniche, Isbn: 123454, Páginas: 247, Preço: 10, Categoria: testes'
    assert_equal @teste_e_design.to_s, result_data_to_s, "#{@teste_e_design.to_s}\n\t is not equal \n#{result_data_to_s}" 
  end

  def test_livro_has_preco
    assert_equal @teste_e_design.preco, 10
  end

  def test_can_change_preco_livro
    @teste_e_design.preco = 20
    assert_equal @teste_e_design.preco, 20
  end

  def test_compare_livro_by_isbn
    design = Livaria::Livro.new "Javaria", "Jose Maria", "123454", 247, 10, :design
    assert @teste_e_design.eql? design
  end

end
