# encoding: utf-8

require 'minitest/autorun'
require_relative './livro.rb'
require_relative './banco_de_arquivos.rb'

class BancoDeArquivosTest < Minitest::Test

  def setup
    @file_name = "livros.yml"

    @categoria =  :testes
    @teste_e_design = Livaria::Livro.new "TDD", "Mauricio Aniche", "123456" , 247, 10, @categoria 
  end

  def teardown 
    File.delete @file_name if File.exist? @file_name
  end

  def test_file_save_yml
    banco_de_arq = Livaria::BancoDeArquivos.new
    banco_de_arq.save @teste_e_design
    assert File.readable? @file_name
  end

  def test_load_livros_from_yml
    banco_de_arq = Livaria::BancoDeArquivos.new
    banco_de_arq.save @teste_e_design
    livros ||= banco_de_arq.carrega 

    assert livros.instance_of? Hash
    assert_equal livros[@categoria], @teste_e_design
  end 

end
