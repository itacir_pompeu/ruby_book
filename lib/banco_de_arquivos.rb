# encoding: utf-8

require 'yaml'

module Livaria
  class BancoDeArquivos 

    def save (livro)
      File.open("livros.yml", "a") do |arquivo|
        arquivo.puts YAML.dump livro
        arquivo.puts ""
      end
    end

    def carrega
      $/ = "\n\n"
      File.open("livros.yml", "r").inject({}) do |hash, arquivo|
        file = YAML.load arquivo 
        hash[file.categoria] = file
        hash
      end
    end

  end
end
