require_relative 'banco_de_arquivos'

module Livaria
  class Biblioteca

    def initialize
      @livros = {} 
      @banco_de_arquivos = Livaria::BancoDeArquivos.new
    end

    def add_livros (livro)
      save livro do 
        @livros[livro.categoria] ||= []
        @livros[livro.categoria] << livro 
      end
    end

    def livros
      @livros ||= @banco_de_arquivos.carrega
      @livros.values.flatten
    end

    def livros_por_categoria (categoria, &bloco)
      @livros[categoria].each do |livro|
        bloco.call livro
      end
    end

    private 

    def save (livro)
      @banco_de_arquivos.save livro
      yield
    end

    #def livros_por_categoria (categoria)
    #  @livros[categoria].map do |livro|
    #    yield livro if block_given? 
    #  end
    #end
  end
end
