# encoding: utf-8

require 'minitest/autorun'
require_relative './biblioteca.rb'
require_relative './livro.rb'
require_relative './relatorio.rb'

class RelatorioTest < Minitest::Test

  def setup
    @teste_e_design = Livaria::Livro.new "TDD", "Mauricio Aniche", "123454", 247, 10, :testes
    @web_design_responsivo = Livaria::Livro.new "Javaria", "Tárcio Zemel", "452565", 189, 69.9, :web_design
    @biblioteca = Livaria::Biblioteca.new
    @biblioteca.add_livros @teste_e_design
    @biblioteca.add_livros @web_design_responsivo

    @relatorio = Livaria::Relatorio.new @biblioteca
  end

  def teardown

  end

  def test_total_of_value_livro
    assert_equal @relatorio.total, 79.90 
  end

  def test_get_title_of_livros
    assert @relatorio.titulos.include? 'TDD'
    assert @relatorio.titulos.include? 'Javaria'
  end
end
